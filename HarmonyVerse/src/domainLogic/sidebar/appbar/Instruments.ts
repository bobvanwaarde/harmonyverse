const Instruments = {
  library: { text: 'Library', slug: 'library', icon: 'mdi-book-open-page-variant' },
  piano: { text: 'Piano', slug: 'piano', icon: 'mdi-piano' },
  guitar: { text: 'Guitar', slug: 'guitar', icon: 'mdi-guitar-acoustic' }, // $vuetify.icons.AcousticGuitarIcon
};

export default Instruments;
