const Chords = {
  major: { text: 'Major', slug: 'major', icon: 'mdi-music' },
  minor: { text: 'Minor', slug: 'minor', icon: 'mdi-music' },
  major7: { text: 'Major 7th', slug: 'major7', icon: 'mdi-music' },
  dominant7: { text: 'Dominant 7th', slug: 'dominant7', icon: 'mdi-music' },
  minor7: { text: 'Minor 7th', slug: 'minor7', icon: 'mdi-music' },
  diminished: { text: 'Diminished', slug: 'diminished', icon: 'mdi-music' },
  augmented: { text: 'Augmented', slug: 'augmented', icon: 'mdi-music' },
};

export default Chords;
