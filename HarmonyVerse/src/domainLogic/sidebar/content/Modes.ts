const Modes = {
  // TODO make modes dependent on Scales
  ionian: { text: 'Ionian', slug: 'ionian', icon: 'mdi-music' },
  dorian: { text: 'Dorian', slug: 'dorian', icon: 'mdi-music' },
  phrygian: { text: 'Phrygian', slug: 'phrygian', icon: 'mdi-music' },
  lydian: { text: 'Lydian', slug: 'lydian', icon: 'mdi-music' },
  mixolydian: { text: 'Mixolydian', slug: 'mixolydian', icon: 'mdi-music' },
  aeolian: { text: 'Aeolian', slug: 'aeolian', icon: 'mdi-music' },
  locrian: { text: 'Locrian', slug: 'locrian', icon: 'mdi-music' },
};

export default Modes;
