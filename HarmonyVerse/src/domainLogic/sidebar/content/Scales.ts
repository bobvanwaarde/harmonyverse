const Scales = {
  chromatic: { text: 'Chromatic', slug: 'chromatic', icon: 'mdi-music' },
  major: { text: 'Major', slug: 'major', icon: 'mdi-music' },
  minor: { text: 'Minor', slug: 'minor', icon: 'mdi-music' },
  harmonicmajor: { text: 'Harmonic Major', slug: 'harmonicmajor', icon: 'mdi-music' },
  harmonicminor: { text: 'Harmonic Minor', slug: 'harmonicminor', icon: 'mdi-music' },
  melodicminor: { text: 'Melodic Minor', slug: 'melodicminor', icon: 'mdi-music' },
};

export default Scales;
