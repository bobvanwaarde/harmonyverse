const Intervals = {
  // TODO fancy names for properties, or keep integers, or make enum?
  0: { text: 'Perfect Unison', slug: '0', icon: 'mdi-music' },
  1: { text: 'Minor 2nd', slug: '1', icon: 'mdi-music' },
  2: { text: 'Major 2nd', slug: '2', icon: 'mdi-music' },
  3: { text: 'Minor 3rd', slug: '3', icon: 'mdi-music' },
  4: { text: 'Major 3rd', slug: '4', icon: 'mdi-music' },
  5: { text: 'Perfect 4th', slug: '5', icon: 'mdi-music' },
  6: { text: 'Diminished 5th', slug: '6', icon: 'mdi-music' },
  7: { text: 'Perfect 5th', slug: '7', icon: 'mdi-music' },
  8: { text: 'Minor 6th', slug: '8', icon: 'mdi-music' },
  9: { text: 'Major 6th', slug: '9', icon: 'mdi-music' },
  10: { text: 'Minor 7th', slug: '10', icon: 'mdi-music' },
  11: { text: 'Major 7th', slug: '11', icon: 'mdi-music' },
  12: { text: 'Perfect Octave', slug: '12', icon: 'mdi-music' },
};

export default Intervals;
