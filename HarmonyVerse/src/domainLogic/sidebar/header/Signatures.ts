const Signatures = {
  flat: { text: '♭', slug: 'flat', icon: 'mdi-music-accidental-flat' },
  sharp: { text: '♯', slug: 'sharp', icon: 'mdi-music-accidental-sharp' },
};

export default Signatures;
