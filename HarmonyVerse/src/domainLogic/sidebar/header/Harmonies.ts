const Harmonies = {
  intervals: { text: 'Intervals', slug: 'intervals', icon: false },
  chords: { text: 'Chords', slug: 'chords', icon: false },
  scales: { text: 'Scales', slug: 'scales', icon: false },
};

export default Harmonies;
