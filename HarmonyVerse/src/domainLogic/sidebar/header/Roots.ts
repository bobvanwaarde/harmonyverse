const Roots = {
  c: { text: 'C', slug: 'c', icon: false },
  d: { text: 'D', slug: 'd', icon: false },
  e: { text: 'E', slug: 'e', icon: false },
  f: { text: 'F', slug: 'f', icon: false },
  g: { text: 'G', slug: 'g', icon: false },
  a: { text: 'A', slug: 'a', icon: false },
  b: { text: 'B', slug: 'b', icon: false },
};

export default Roots;
