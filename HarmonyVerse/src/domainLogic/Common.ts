export function getElementDimensions(el: any) {
  return {
    width: el.getBoundingClientRect().width,
    height: el.getBoundingClientRect().height,
  };
}

export function getViewPort() {
  return getElementDimensions(document.body);
}
