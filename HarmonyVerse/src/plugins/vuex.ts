import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

// todo define lists of possible instruments, harmonies, roots, signatures, scales, modes, chords, intervals

export default new Vuex.Store({
  state: {
    count: 0,
    instrument: undefined,
    harmony: undefined,
    root: undefined,
    signature: undefined,
    chord: undefined,
    scale: undefined,
    mode: undefined,
    interval: undefined,
  },
  mutations: {
    increment(state) {
      state.count++;
    },
    setInstrument(state, instrument) {
      state.instrument = instrument;
    },
    setHarmony(state, harmony) {
      // console.log('store harmony change');
      state.harmony = harmony;
    },
    setRoot(state, root) {
      // console.log('store root change');
      state.root = root;
    },
    setSignature(state, signature) {
      // console.log('store signature change');
      state.signature = signature;
    },
    setInterval(state, interval) {
      // console.log('store interval change');
      state.interval = interval;
    },
    setChord(state, chord) {
      // console.log('store chord change');
      state.chord = chord;
    },
    setScale(state, scale) {
      // console.log('store scale change');
      state.scale = scale;
    },
    setMode(state, mode) {
      // console.log('store mode change');
      state.mode = mode;
    },
  },
  getters: {
    //
  },
});
