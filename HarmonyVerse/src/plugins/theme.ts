import colors from 'vuetify/lib/util/colors';

export default {
    light: {
        primary: colors.indigo.accent1,
        secondary: colors.indigo.base,
        tertiary: colors.pink.base,
        success: colors.purple.accent1,
        warning: colors.yellow.accent1,
        sidebar: colors.grey.lighten4,
        sidebarbutton: colors.yellow.accent1,
        mainScreen: colors.blueGrey.lighten1,
    },
    dark: {
        primary: colors.indigo.darken1,
        secondary: colors.indigo.darken4,
        tertiary: colors.pink.darken1,
        success: colors.pink.accent2,
        warning: colors.purple.accent2,
        sidebar: colors.grey.darken3,
        sidebarbutton: colors.pink.darken3,
        mainScreen: '#130021',
    },
};
