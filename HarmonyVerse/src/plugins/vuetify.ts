import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import themes from './theme';
import customIcons from '@/icons/customIcons';

Vue.use(Vuetify);

export default new Vuetify({
    theme: {
        dark: true,
        themes,
        options: { customProperties: true },
    },
    icons: customIcons,
});
