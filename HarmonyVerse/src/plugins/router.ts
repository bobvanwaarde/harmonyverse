import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import store from '@/plugins/vuex';

import Harmonies from '@/domainLogic/sidebar/header/Harmonies';
import Instruments from '@/domainLogic/sidebar/appbar/Instruments';

import HomeView from '@/views/Home.vue';
import IntervalsView from '@/views/Intervals/Index.vue';
import ChordsView from '@/views/Chords/Index.vue';
import ScalesView from '@/views/Scales/Index.vue';
import AboutView from '@/views/About.vue';

Vue.use(VueRouter);

const HarmoniesList = Object.values(Harmonies).map(h => h.slug);
const instrumentList = Object.values(Instruments).map(i => i.slug);

const routes: RouteConfig[] = [
  // todo fix scales and modes
  // todo use lists of instruments, harmonies, roots, signatures,
  //      scales, modes, chords, intervals in regex
  // todo Harmonies.<thing>.slug -> harmonySlug ?
  {
    path: '/about',
    name: 'About',
    component: AboutView,
  },
  {
    path: '/:instrumentSlug(' + instrumentList.join('|') + ')?'
    +     '/:rootSlug([abcdefg])?'
    +     '/:signatureSlug(flat|sharp)?',
    // name: HarmonyType.all,
    // name: Harmonies.all.slug,
    name: 'Home',
    props: true,
    component: HomeView,
  },
  {
    path: '/:instrumentSlug(' + instrumentList.join('|') + ')?'
    +     '/:rootSlug([abcdefg])?'
    +     '/:signatureSlug(flat|sharp)?'
    +     '/' + Harmonies.chords.slug + '/:chordSlug?',
    // name: HarmonyType.chords,
    name: Harmonies.chords.slug,
    props: true,
    component: ChordsView,
  },
  /*
  {
    path: '/:rootSlug([abcdefg])?/:signatureSlug(|flat|sharp)?/' + HarmonyType.scales + '/:scaleSlug?',  // ' ((/:scaleSlug?)|(/:scaleSlug/modes/:modeSlug?))',
    name: HarmonyType.scales,
    props: true,
    component: ScalesView,
  },
  */
  {
    path: '/:instrumentSlug(' + instrumentList.join('|') + ')?'
    +     '/:rootSlug([abcdefg])?'
    +     '/:signatureSlug(flat|sharp)?'
    +     '/' + Harmonies.scales.slug + '/:scaleSlug?/:modeSlug?',
    // name: HarmonyType.scales,
    name: Harmonies.scales.slug,
    props: true,
    component: ScalesView,
  },
  {
    path: '/:instrumentSlug(' + instrumentList.join('|') + ')?'
    +     '/:rootSlug([abcdefg])?'
    +     '/:signatureSlug(flat|sharp)?'
    +     '/' + Harmonies.intervals.slug + '/:intervalSlug?',
    name: Harmonies.intervals.slug,
    props: true,
    component: IntervalsView,
  },
];

const router = new VueRouter({
  routes,
});


router.beforeEach( (to, from, next) => {
    // console.log('router update');

    const routeName: any = to.name;

    if (HarmoniesList.includes(routeName)) { store.commit('setHarmony', routeName ); }
    if (routeName === 'Home') { store.commit('setHarmony', '' ); }

    store.commit('setInstrument', to.params.instrumentSlug);
    store.commit('setRoot', to.params.rootSlug);
    store.commit('setSignature', to.params.signatureSlug);
    store.commit('setInterval', to.params.intervalSlug);
    store.commit('setChord', to.params.chordSlug);
    store.commit('setScale', to.params.scaleSlug);
    store.commit('setMode', to.params.modeSlug);

    // console.log(to.name);
    // console.log(to.params);

    next();
  },
);


export default router;
