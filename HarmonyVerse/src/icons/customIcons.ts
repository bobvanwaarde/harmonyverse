import AcousticGuitarIcon from '@/icons/acousticGuitarIcon.vue';
import ElectricGuitarIcon from '@/icons/electricGuitarIcon.vue';
import FretBoardIcon from '@/icons/fretBoardIcon.vue';
import HarmonyVerseIcon from '@/icons/harmonyVerseIcon.vue';

export default {
    values: {
        AcousticGuitarIcon: { component: AcousticGuitarIcon },
        ElectricGuitarIcon: { component: ElectricGuitarIcon },
        FretBoardIcon: { component: FretBoardIcon },
        HarmonyVerseIcon: { component: HarmonyVerseIcon },
    },
};
